package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.component.Bootstrap;
import com.tsc.jarinchekhina.tm.endpoint.Project;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.endpoint.Status;
import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    private static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private static Session session;

    @BeforeClass
    public static void before() {
        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
        session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
        Assert.assertNotNull(session);
    }

    @AfterClass
    public static void after() {
        bootstrap.getProjectEndpoint().clearProjects(session);
        @NotNull final Session adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
        bootstrap.getSessionEndpoint().closeSession(adminSession);
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCreateFindRemove() {
        bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        bootstrap.getProjectEndpoint().createProject(session, "AUTO Project 2");
        bootstrap.getProjectEndpoint().createProjectWithDescription(session, "AUTO Project 3", "Desc");
        @NotNull List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(3,listProjects.size());

        @NotNull String projectId = listProjects.get(0).getId();
        @NotNull final Project projectById = bootstrap.getProjectEndpoint().findProjectById(session, projectId);
        Assert.assertEquals(listProjects.get(0).getName(), projectById.getName());
        @NotNull final Project projectByIndex = bootstrap.getProjectEndpoint().findProjectByIndex(session, 2);
        Assert.assertEquals(listProjects.get(1).getName(), projectByIndex.getName());
        @NotNull final Project projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 3");
        Assert.assertNotNull(projectByName);

        projectId = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project").getId();
        bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        bootstrap.getProjectEndpoint().removeProjectByName(session, "AUTO Project 3");
        bootstrap.getProjectEndpoint().removeProjectByIndex(session, 1);
        listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(0,listProjects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testUpdate() {
        bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");

        @NotNull String projectId = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project").getId();
        bootstrap.getProjectEndpoint().updateProjectById(session, projectId, "AUTO","DESC");
        @NotNull Project project = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO");
        Assert.assertEquals("AUTO", project.getName());
        Assert.assertEquals("DESC", project.getDescription());

        bootstrap.getProjectEndpoint().updateProjectByIndex(session, 1, "JUNIT","JDESC");
        project = bootstrap.getProjectEndpoint().findProjectByName(session, "JUNIT");
        Assert.assertEquals("JDESC", project.getDescription());

        bootstrap.getProjectEndpoint().removeProjectByIndex(session, 1);
        @NotNull final List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(0,listProjects.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testChangeStatus() {
        bootstrap.getProjectEndpoint().createProject(session, "AUTO Project");
        bootstrap.getProjectEndpoint().createProjectWithDescription(session, "AUTO Project 2", "Desc");

        @NotNull Project projectById = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project");
        @NotNull String projectId = projectById.getId();
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
        bootstrap.getProjectEndpoint().startProjectById(session, projectId);
        projectById = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project");
        Assert.assertEquals(Status.IN_PROGRESS, projectById.getStatus());
        bootstrap.getProjectEndpoint().finishProjectById(session, projectId);
        projectById = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project");
        Assert.assertEquals(Status.COMPLETED, projectById.getStatus());
        bootstrap.getProjectEndpoint().changeProjectStatusById(session, projectId, Status.NOT_STARTED);
        projectById = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project");
        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());

        @NotNull Project projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 2");
        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());
        bootstrap.getProjectEndpoint().startProjectByName(session, "AUTO Project 2");
        projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 2");
        Assert.assertEquals(Status.IN_PROGRESS, projectByName.getStatus());
        bootstrap.getProjectEndpoint().finishProjectByName(session, "AUTO Project 2");
        projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 2");
        Assert.assertEquals(Status.COMPLETED, projectByName.getStatus());
        bootstrap.getProjectEndpoint().changeProjectStatusByName(session, "AUTO Project 2", Status.NOT_STARTED);
        projectByName = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project 2");
        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());

        projectId = bootstrap.getProjectEndpoint().findProjectByName(session, "AUTO Project").getId();
        bootstrap.getProjectEndpoint().removeProjectById(session, projectId);
        bootstrap.getProjectEndpoint().removeProjectByName(session, "AUTO Project 2");
        @NotNull final List<Project> listProjects = bootstrap.getProjectEndpoint().findAllProjects(session);
        Assert.assertEquals(0,listProjects.size());
    }

}
